$(function () {

    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        closeButton: 'outside',
    });

    let lastScrollTop = 0;









    $(window).resize(function () {
        fixedHeader()
    })

    $('.js_done_project__slider__block__front').click(function () {
        $block = $('.js_done_project__slider__block')
        $block.addClass('full')
        setTimeout(() => {
            $('.js_done_project__slider__about').slick('slickGoTo', 0);
            $("html, body").animate({
                scrollTop: $('.js_done_project__slider__block').offset().top
            }, 750);
        }, 750);

    })









    $('.js_btn_clear_filter').click(function () {
        $(this).closest('form').find('select').each(function () {
            $(this).val('Все').trigger("change");
        });
    })



    let inputPhone = document.querySelectorAll(".js__phone_mask");

    let im = new Inputmask("+7 (999)-999-99-99");

    inputPhone.forEach((item) => {
        im.mask(inputPhone);
    });

    function validateEmail(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }


    function validateForm($form) {
        let validate = true;
        // for simple input
        $form.find('input[required][type="text"],textarea[required]').each(function () {
            if ($(this).val().length <= 0 && !($(this).hasClass('js__phone_mask'))) {
                $(this).addClass('warning')
                $(this).after(`<div class='error_label js_error_label'>*Заполните поле</div>`)
                validate = false;
            }
        })

        // for phone input
        $form.find('.js__phone_mask').each(function () {
            if ($(this).val().slice(-1) == '_' || $(this).val().length <= 0) {
                $(this).addClass('warning')
                $(this).after(`<div class='error_label js_error_label'>*Заполните поле</div>`)
                validate = false;
            }
        })

        // for email input
        $form.find('input[type="mail"]').each(function () {
            if (!validateEmail($(this).val())) {
                $(this).addClass('warning')
                $(this).after(`<div class='error_label js_error_label'>*Заполните поле</div>`)
                validate = false;
            }
        })

        // for select

        return validate;
    }


    $('.js_form input,.js_form textarea').focus(function (e) {
        e.preventDefault();
        $(this).removeClass('warning')
        if ($(this).next().hasClass('js_error_label')) {
            $(this).next().remove()
        }

    });



    $('.js_btn__submit').click(function (e) {
        e.preventDefault();
        let $form = $(this).closest('form')
        $form.find('input[required],textarea[required]').removeClass('warning')
        $form.find('.js_error_label').remove()

        if (validateForm($form)) {
            alert('all good send ajax request')
        }
    });




    $(".js_select").each(function (index, element) {
        let placeholder = $(this).attr('placeholder');
        $(this).select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
        })
    });


    $('.js_portfolio_filter__title').click(function () {
        $('.js_portfolio_filter__title').toggleClass('active')
        if ($('.js_portfolio_filter__title').hasClass('active')) {
            $('.js_portfolio_filter__form').slideUp(400)
        } else {
            $('.js_portfolio_filter__form').slideDown(400)
        }
    })


    $('.js_page__project__info .page__project__info__title').click(function () {
        $(this).closest('.js_page__project__info').toggleClass('active')
        $(this).closest('.js_page__project__info').find('.page__project__info__list').stop().slideToggle(400)
    })







    $(window).on("load", function () {
        $('body').addClass('loaded')
    })



    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });

    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });

    $('.js_btn_modal_call').click(function (e) {
        Fancybox.show([{
            src: "#js_modal__call"
        }])
    });



    $('.js_header__city .header__city__value').click(function () {
        $(this).closest('.js_header__city').toggleClass('active')
    })


    $('.js_tab_section__list .tab_section__item__top').click(function (e) {
        e.preventDefault();
        $(this).closest('.tab_section__item').find('.tab_section__item__content').slideToggle(400, function () {
            $(this).closest('.tab_section__item').toggleClass('active')
        })

    })

    $('.js_has_submenu').click(function () {
        $(this).toggleClass('active').find('.menu_submenu').stop().slideToggle(500)
    })



    $('.js_btn_menu,.js_modal__menu__btn_close').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
    });

    $('.js_btn_menu--desktop').click(function (e) {
        e.preventDefault();
        let $header = $('.js_header_main');
        if (!$('.js_btn_menu--desktop').hasClass('active')) {
            $header.addClass('open_menu--desktop')
            $('.js_btn_menu--desktop').addClass('active')
            $('.js__modal__menu--desktop').addClass('active')
        } else {
            $header.removeClass('open_menu--desktop')
            $('.js_btn_menu--desktop').removeClass('active')
            $('.js__modal__menu--desktop').removeClass('active')
        }
    });


    function fixedHeader() {

        let $header = $('.js_header_main');
        let st = $(this).scrollTop();

        if ($(window).scrollTop() > $header.outerHeight()) {
            $('body').css('padding-top', $header.outerHeight())
            $header.addClass('fixed')
        } else {
            if ($header.hasClass('fixed')) {
                $('body').css('padding-top', '0')
                $header.removeClass('fixed fixed_up')
                $header.removeClass('open_menu--desktop')
                $('.js_btn_menu--desktop').removeClass('active')
                $('.js__modal__menu--desktop').removeClass('active')
            }
        }

        if (st > lastScrollTop) {
            if ($header.hasClass('fixed')) {
                $header.removeClass('fixed_up')
            }
        } else {
            if ($header.hasClass('fixed')) {
                $header.addClass('fixed_up')
            }
        }


    }





    $('.js_service__slider').slick({
        slidesToShow: 1,
        arrows: true,
        dots: true
    });


    $('.js_done_project__slider_x').slick({
        slidesToShow: 1,
        arrows: true,
        dots: false,
    });


    $('.js_done_project__slider--mobile').slick({
        slidesToShow: 1,
        arrows: false,
        dots: true
    });

    $('.js_home__promo__slider').slick({
        arrows: false,
        dots: true,
        infinite: false,
        fade: true,
    });


    $('.js_another_article__list').slick({
        dots: true,
        infinite: true,
        arrows: true,
        speed: 300,
        slidesToShow: 1,
        variableWidth: true
    })




    $('.js_done_project__slider').slick({
        arrows: true,
        dots: true,
        infinite: false,
    })

    $('.js_done_project__slider__about').slick({
        arrows: true,
        dots: true,
        infinite: false,
    })



    $('.js_done_project__slider__about').on('afterChange', function (event, slick, currentSlide, nextSlide) {



        if (currentSlide + 1 == slick.slideCount) {
            slick.$slider.closest('.js_done_project__slider_about__section').addClass('show_btn')
        } else {
            slick.$slider.closest('.js_done_project__slider_about__section').removeClass('show_btn')
        }

    });


    $(window).scroll(function () {
        fixedHeader()

        let st = $(this).scrollTop();

        if ($(window).width() > 750 && $('.js_done_project__slider__block').length > 0) {

            if (st < lastScrollTop) {
                if ($('.js_done_project__slider_about__section.done_project__slider_about__section--first').hasClass('active') && $('.js_done_project__slider__block').hasClass('full')) {
                    if ($('.js_done_project__slider__block__for_view').offset().top  >= $(window).scrollTop()) {
                        setTimeout(() => {
                            let $block = $('.js_done_project__slider__block');
                            $block.removeClass('full');
                            $('.js_done_project__slider__about').slick('slickGoTo', 0);
                        }, 500);
                    }
                }
            }
        }
        lastScrollTop = $(this).scrollTop();
    })


    $('.js_home__promo__slider').mousemove(function (e) {
        if ($(window).width() >= 900) {
            let fullWidth = $('.js_home__promo__slider').outerWidth()
            let countSection = $('.home__promo__slide').length;
            let sizeSection = fullWidth / countSection
            let x = e.pageX - this.offsetLeft;
            let chooseSectionID = Math.trunc(x / sizeSection);

            $('.js_home__promo__slider').slick('slickGoTo', chooseSectionID);
        }
    });







    let blockedForBtn = false;
    let blockTimeout = null;

    let blocked = false;


    let scrollOnSection = true

    $(".js_done_project__slider__block").on('mousewheel DOMMouseScroll wheel', (function (e) {

        scrollOnSection = true;

        if ($(this).hasClass('full')) {
            let deltaY = e.originalEvent.deltaY;
            let currentSlide = $('.js_done_project__slider_about__section.active')


            clearTimeout(blockTimeout);
            blockTimeout = setTimeout(function () {
                blocked = false;
            }, 50);


            if (!blocked) {

                blocked = true;
                if (deltaY > 0) {
                    if ($('.js_done_project__slider_about__section.active').next().length > 0) {
                        $('.js_done_project__slider_about__section.active').next().addClass('active')
                        currentSlide.removeClass('active')
                        $("html, body").animate({
                            scrollTop: $('.js_done_project__slider_about__section.active').offset().top
                        }, 750, function () {
                            scrollOnSection = false;
                        });
                    }

                } else {
                    if ($('.js_done_project__slider_about__section.active').prev().length > 0 && $('.js_done_project__slider_about__section.active').prev().hasClass('js_done_project__slider_about__section')) {
                        $('.js_done_project__slider_about__section.active').prev().addClass('active')
                        currentSlide.removeClass('active')
                        $("html, body").animate({
                            scrollTop: $('.js_done_project__slider_about__section.active').offset().top
                        }, 750, function () {
                            scrollOnSection = false;
                        });
                    }

                }
            }



            if (!scrollOnSection) {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    }));









    $('.js_default__text__slider').slick({
        arrows: true,
        dots: true
    })

    if ($('.js_slider_review').length) {
        sliderReview()
    }


    if ($('.js_stage_block__slider').length) {
        sliderStage()
    }






    function sliderStage() {

        let sliderParent = $('.js_stage_block__slider')

        function currentAndMaxSlider(currrentSlide) {
            let $companyMax = $('.stage_block__max');
            let $companyCurrent = $('.stage_block__current')
            let countSlides = sliderParent.find('.stage_block__slide').length;


            $companyMax.html(countSlides)
            $companyCurrent.html(currrentSlide)

            let lineWidth = 100 / (parseInt(countSlides) / parseInt(currrentSlide))

            sliderParent.closest('.stage_block').find('.js_stage_block__line .stage_block__line__title').html('Этап ' + currrentSlide)
            sliderParent.closest('.stage_block').find('.js_stage_block__line .stage_block__line__x__value').css('width', lineWidth + '%')


        }

        sliderParent.on('init', function (event, slick, currentSlide, nextSlide) {
            currentAndMaxSlider(1)

        });

        sliderParent.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            currentAndMaxSlider(currentSlide + 1)
        });

        sliderParent.slick({
            infinite: false,
            prevArrow: $('.js_stage__arrow .btn_stage__arrow--prev'),
            nextArrow: $('.js_stage__arrow .btn_stage__arrow--next'),
            slidesToShow: 1,
        })

    }

    function sliderReview() {

        let sliderParent = $('.js_slider_review')

        function currentAndMaxSlider(currrentSlide) {
            let $companyMax = $('.block_review__slider__info_max');
            let $companyCurrent = $('.block_review__slider__info_current')
            let countSlides = sliderParent.find('.block_review__item').length;

            $companyMax.html(countSlides)
            $companyCurrent.html(currrentSlide)
        }

        sliderParent.on('init', function (event, slick, currentSlide, nextSlide) {
            currentAndMaxSlider(1)

        });

        sliderParent.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            currentAndMaxSlider(currentSlide + 1)
        });

        sliderParent.slick({
            infinite: false,
            prevArrow: $('.js_btn_review__slider__arrow.btn_review__slider__arrow--prev'),
            nextArrow: $('.js_btn_review__slider__arrow.btn_review__slider__arrow--next'),
            slidesToShow: 1,
        })

    }




    $(".js__animate__left").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0.25;

        gsap.from(el, 1, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%'
            },
            x: -100,
            opacity: 0,
            delay: delayTime,
            stagger: {
                amount: 0.3
            }
        })
    })

    $(".js__animate__right").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0.25;

        gsap.from(el, 1, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%'
            },
            x: 100,
            opacity: 0,
            delay: delayTime,
            stagger: {
                amount: 0.3
            }
        })
    })

    $(".js__animate__top").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0.25;

        gsap.from(el, 1, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%'
            },
            y: 50,
            opacity: 0,
            delay: delayTime,
            stagger: {
                amount: 0.3
            }
        })
    })









    $(".js__animate__bottom").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0.25;

        gsap.from(el, 1, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%'
            },
            y: -50,
            opacity: 0,
            delay: delayTime,
            stagger: {
                amount: 0.3
            }
        })
    })


    $(".js__animate__opacity").each(function () {
        let el = $(this)[0]
        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0.25;

        gsap.from(el, 1, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%'
            },
            opacity: 0,
            delay: delayTime,
            stagger: {
                amount: 0.3
            }
        })
    })






    $(".js__animate__scale").each(function () {
        el = $(this)[0];
        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0.25;

        gsap.from(el, 1, {
            scrollTrigger: {
                trigger: $(el).closest('.modern__marketing')[0],
                start: 'top 50%'
            },
            scale: 0,
            opacity: 0,
            delay: delayTime,
            stagger: {
                amount: 0.3
            }
        })
    })







});